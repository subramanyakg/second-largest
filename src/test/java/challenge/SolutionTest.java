package challenge;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SolutionTest {
    private static final Integer INVALID = Integer.MIN_VALUE;

    @Test
    void testNullInputList() {
        int secondLargestNumber = Solution.findSecondLargestNumber(null);
        assertEquals(INVALID, secondLargestNumber);
    }

    @Test
    void testEmptyInputList() {
        List<Integer> input = Collections.emptyList();
        int secondLargestNumber = Solution.findSecondLargestNumber(input);
        assertEquals(INVALID, secondLargestNumber);
    }

    @Test
    void testFindSecondLargestNumber() {
        List<Integer> input = Arrays.asList(0, 1, 4, 10, 15);
        int expected = 10;
        int secondLargestNumber = Solution.findSecondLargestNumber(input);
        assertEquals(expected, secondLargestNumber);
    }

    @Test
    void testFindSecondLargestNumberForNegativeIntegers() {
        List<Integer> input = Arrays.asList(-12, -35, -1, -10, -34, -1);
        int expected = -10;
        int secondLargestNumber = Solution.findSecondLargestNumber(input);
        assertEquals(expected, secondLargestNumber);
    }

}
