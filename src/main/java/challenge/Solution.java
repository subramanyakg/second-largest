package challenge;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Solution {
    private static final Logger LOGGER = Logger.getLogger(Solution.class.getName());
    /**
     * Find the second largest number in the list
     * @param input list of numbers
     * @return second largest number or Integer.MIN_VALUE in case of wrong input
     */
    public static int findSecondLargestNumber(List<Integer> input) {
        int i, first, second;
        first = second = Integer.MIN_VALUE;
        if (input == null || input.size() < 2) {
            LOGGER.severe("Invalid Input provided");
            return second;
        }
        for (i = 0; i < input.size(); i++) {
            // if i th element is < first, then
            if (input.get(i) > first) {
                second = first;
                first = input.get(i);
            }
            // if i th element is, first > ith > second
            else if (input.get(i) > second && input.get(i) != first) {
                second = input.get(i);
            }
        }
        LOGGER.log(Level.INFO, "Second Largest Number is:{0}", second);
        return second;
    }
}
